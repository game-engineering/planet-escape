extends Sprite2D

var old_rot
var time : float

func _ready():
	old_rot = rotation

func _physics_process(delta):
	time += delta
	rotation = old_rot + sin(time) * 0.2
