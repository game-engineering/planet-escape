extends ColorRect

func _ready():
	visible = true
	

func fade_to_game():
	color.a = 1
	await get_tree().create_timer(0.3).timeout
	var tween = create_tween()
	tween.tween_property(self, "color:a", 0, 0.3)
	await tween.finished
	visible = false

func black():
	color.a = 1
	visible = true

func fade_to_black_and_change_to_scene(scene: PackedScene):
	color.a = 0
	visible = true
	var tween = create_tween()
	tween.tween_property(self, "color:a", 1, 0.3)
	await tween.finished
	get_tree().change_scene_to_packed(scene)

func fade_to_black_and_reload_scene():
	color.a = 0
	visible = true
	var tween = create_tween()
	tween.tween_property(self, "color:a", 1, 0.3)
	await tween.finished
	get_tree().reload_current_scene()
	
