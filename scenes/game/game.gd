extends Node2D

@onready var player = %Player
@onready var player_camera : Camera2D = %PlayerCamera
@onready var void_camera = %VoidCamera
@onready var planet_camera = %PlanetCamera
@onready var moving_camera = %MovingCamera
@onready var planet_burst_particles = $Always/Void/PlanetBurstParticles
@onready var black_screen = $Always/CanvasLayer/BlackScreen
@onready var air_indicator = $Always/CanvasLayer/AirIndicator
@onready var the_void = $Always/Void
@onready var preload_timer = %PreloadTimer
@onready var goal_message = $Always/CanvasLayer/GoalMessage
@onready var final_player_spawn_point = $Pausable/FinalPlayerSpawnPoint


@onready var mobile_joystick = $Always/CanvasLayer/MobileJoystick
@onready var boost_button = $Always/CanvasLayer/BoostButton
@onready var switch_cam_button = $Always/CanvasLayer/SwitchCamButton
@onready var reset_button = $Always/CanvasLayer/ResetButton

var cam_tween : Tween
var air_tween : Tween


@onready var first_air_supply = %FirstAirSupply
@onready var concerned_citizen = %ConcernedCitizen

@onready var thanks_message = %ThanksMessage


@onready var cams = [player_camera, planet_camera, void_camera]

func _ready():
	mobile_joystick.visible = globals.mobile_controls
	boost_button.visible = globals.mobile_controls
	switch_cam_button.visible = globals.mobile_controls
	reset_button.visible = globals.mobile_controls
	get_tree().paused = false
	if globals.goal_reached:
		#print("Goal reached")
		thanks_message.visible = true
		player.global_position = final_player_spawn_point.position
		first_air_supply.queue_free()
		concerned_citizen.message = "Our planet is in danger! We need to find a solution here and now. I am sure, we can make it, but this is for another Game Jam. Thank you for playing!"
		concerned_citizen.hide_time = 5
		the_void.speed = 0
		
	if globals.first_time_loaded:
		_preload_particles([the_void.burst_particles, planet_burst_particles])
	else:
		black_screen.fade_to_game()
	for planet : Planet in get_tree().get_nodes_in_group("planets"):
		planet.planet_killed.connect(_planet_killed)
	player.air_changed.connect(func(value):
			if air_tween:
				air_tween.kill()
			if abs(air_indicator.value - value) > 10: 
				air_tween = create_tween()
				air_tween.tween_property(air_indicator, "value", value, 0.3)
			else:
				air_indicator.value = value
			)
	player.player_died.connect(_reset_game)


func _preload_particles(particles : Array[GPUParticles2D]):
	#black_screen.visible = false
	var old_positions : Array[Vector2] = []
	var old_cam = get_viewport().get_camera_2d()
	moving_camera.global_position = Vector2(-10000,-10000)
	moving_camera.make_current()
	for p in particles:
		old_positions.append(p.global_position)
		p.global_position = Vector2(-10000,-10000)
		p.emitting = true
	preload_timer.wait_time = 2
	preload_timer.start()
	await preload_timer.timeout
	preload_timer.wait_time = 10
	preload_timer.start()
	preload_timer.timeout.connect(func():
		for i in range(len(particles)):
			particles[i].global_position = old_positions[i]	
		)
	old_cam.make_current()
	black_screen.fade_to_game()
	globals.first_time_loaded = false

func _planet_killed(planet: Planet):
	get_tree().paused = true
	var oldcam = get_viewport().get_camera_2d()
	await goto_cam(void_camera, 0.5).cam_ready
	await get_tree().create_timer(2).timeout
	planet_burst_particles.global_position = planet.global_position
	planet_burst_particles.process_material.emission_sphere_radius = planet.size
	planet_burst_particles.emitting = true
	await get_tree().create_timer(2).timeout
	var tween = create_tween().set_trans(Tween.TRANS_BOUNCE).set_ease(Tween.EASE_OUT_IN)
	tween.tween_property(planet, "modulate", Color.BLACK, 2)
	tween.tween_callback(planet.queue_free)
	await get_tree().create_timer(4).timeout
	goto_cam(oldcam)
	get_tree().paused = false
	

func _input(event):
	if event.is_action_pressed("debug"):
		print("Debug trigger")
	if event.is_action_pressed("Cam Planet"):
		goto_cam(planet_camera)
	elif event.is_action_pressed("Cam Player"):
		goto_cam(player_camera)
	elif event.is_action_pressed("Cam Void"):
		goto_cam(void_camera)
	elif event.is_action_pressed("Switch Cam"):
		_on_switch_cam_button_pressed()
	elif event.is_action_pressed("Reset"):
		_on_reset_button_pressed()
	elif event.is_action_pressed("Start") and globals.goal_reached:
		_on_restart_button_pressed()
		
		
func _interpolate_to_moving_cam(value: float, cam1: Camera2D, cam2: Camera2D, startpos: Vector2):
	cam1.global_position = lerp(startpos, cam2.global_position, value)
		
func goto_cam(newcam: Camera2D, speed=1.0) -> CamWait:
	var cam_wait = CamWait.new()
	var oldcam = get_viewport().get_camera_2d()
	if oldcam != moving_camera:
		moving_camera.global_position = oldcam.global_position
		moving_camera.zoom = oldcam.zoom
		moving_camera.make_current()
	if cam_tween:
		cam_tween.kill()
	cam_tween = create_tween().set_parallel() #.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
	cam_tween.tween_method(_interpolate_to_moving_cam.bind(moving_camera, newcam, moving_camera.global_position), 0.0, 1.0, speed)
	cam_tween.tween_property(moving_camera, "zoom", newcam.zoom, speed)
	cam_tween.chain().tween_callback(func(): 
		var preserve_smoothing = newcam.position_smoothing_enabled 
		if preserve_smoothing:
			newcam.position_smoothing_enabled = false
		newcam.make_current()
		if preserve_smoothing:
			newcam.position_smoothing_enabled = true
		cam_wait.cam_ready.emit()
		)
	return cam_wait
	
	

func _on_mobile_joystick_joystick_moved(direction : Vector2):
	#print(direction)
	player.joy_direction = direction.x
	


func _on_boost_button_pressed():
	var jump_event = InputEventKey.new()
	jump_event.physical_keycode = KEY_W
	jump_event.pressed = true
	Input.parse_input_event(jump_event)


func _on_boost_button_released():
	var jump_event = InputEventKey.new()
	jump_event.physical_keycode = KEY_W
	jump_event.pressed = false
	Input.parse_input_event(jump_event)


func _on_player_gravity_center_changed(gravity_center):
	var tween = create_tween()
	tween.tween_property(planet_camera, "global_position", gravity_center, 1)


func _on_switch_cam_button_pressed():
	var current_index = cams.find(get_viewport().get_camera_2d())
	if current_index > -1:
		goto_cam(cams[(current_index + 1) % cams.size()])


func _on_reset_button_pressed():
	_reset_game()

func _reset_game():
	preload_timer.stop()
	black_screen.fade_to_black_and_reload_scene()

class CamWait:
	signal cam_ready


func _on_final_goal_body_entered(_body):
	if player.died:
		return
	globals.goal_reached = true
	player.gravity_velocity = Vector2(0,0)
	player.air_consumption = 0
	await get_tree().create_timer(2).timeout
	await goto_cam(void_camera).cam_ready
	goal_message.modulate.a = 0
	goal_message.visible=true
	var tween = create_tween()
	tween.tween_property(goal_message, "modulate:a", 1, 0.3)
	await get_tree().create_timer(5).timeout
	await goto_cam(player_camera).cam_ready
	await get_tree().create_timer(3).timeout
	player.kill()


func _on_prevent_void_area_body_entered(body):
	if body is Player:
		the_void.enabled = false



func _on_restart_button_pressed():
	black_screen.fade_to_black_and_change_to_scene(globals.TITLE)
