extends Node2D
@onready var slime = $Slime
@onready var air_supply = $AirSupply
@onready var planet_2 = $".."


func _ready():
	air_supply.visible = false
	slime.slime_died.connect(func():
			air_supply.visible = true
			planet_2.atmosphere = 100
			)
