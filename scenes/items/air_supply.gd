extends Node2D

@onready var audio_stream_player_2d = $AudioStreamPlayer2D


func _on_area_2d_body_entered(body):
	if visible and body is Player:
		audio_stream_player_2d.play()
		body.air += 20
		visible = false
		await audio_stream_player_2d.finished
		queue_free()
