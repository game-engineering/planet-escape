extends Area2D

@onready var gravity_field_collision_shape = $GravityFieldCollisionShape

@export var color: Color = Color.BLUE

func _draw():
	draw_circle(position, gravity_field_collision_shape.shape.radius, color)
