@tool
class_name Planet
extends AnimatableBody2D

signal planet_killed(planet: Planet)

@onready var polygon_2d = $Polygon2D
@onready var collision_polygon_2d = $CollisionPolygon2D
@onready var gravity_field = %GravityField
@onready var gravity_field_collision_shape = $GravityField/GravityFieldCollisionShape


@export var rotation_speed := 10
@export var size := 300
@export var random_offset = 20

@export var atmosphere := 100:
	set(value):
		atmosphere = value
		if gravity_field_collision_shape:
			gravity_field_collision_shape.shape.radius = size + atmosphere
			gravity_field.queue_redraw()
			
@export var gravity : float = 980:
	set(value):
		if gravity_field:
			gravity_field.gravity = value

@export var gradient_point: float = 0.5:
	set(value):
		gradient_point = value
		if polygon_2d:
			var gradient = polygon_2d.texture.color_ramp as Gradient
			gradient.offsets[0] = value
			gradient.offsets[1] = value

@export var color1: Color = Color.html("#ad7861"):
	set(value):
		color1 = value
		if polygon_2d:
			(polygon_2d.texture.color_ramp as Gradient).colors[0] = value

@export var color2: Color = Color.html("#cf9667"):
	set(value):
		color2 = value
		if polygon_2d:
			(polygon_2d.texture.color_ramp as Gradient).colors[1] = value

# Called when the node enters the scene tree for the first time.
func _ready():
	gravity = gravity
	atmosphere = atmosphere
	color1 = color1
	color2 = color2
	gradient_point = gradient_point
	polygon_2d.texture.noise.seed = randi_range(1, 1000)
	@warning_ignore("integer_division")
	collision_polygon_2d.polygon = create_circle(size, 32, random_offset)
	polygon_2d.polygon = collision_polygon_2d.polygon
	gravity_field_collision_shape.shape.radius = size + atmosphere

func _enter_tree():
	rotation_degrees = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if not Engine.is_editor_hint():
		rotation_degrees = fmod(rotation_degrees + rotation_speed * delta, 360) 


@warning_ignore("shadowed_variable")
func create_circle(radius: int, points: int, random_offset = 0):
	var point:= Vector2(0, radius)
	var polygon = PackedVector2Array()
	for i in range(points):
		point = point.rotated(2*PI/points)
		polygon.append(point + Vector2(randf_range(0, random_offset), randf_range(0, random_offset)))
	return polygon


func _on_gravity_field_body_entered(body: Node2D):
	if body.has_method("set_gravity"):
		body.set_gravity(position + gravity_field.gravity_point_center, gravity, rotation_speed)
		



func _on_gravity_field_body_exited(body):
	if body.has_method("set_gravity"):
		var new_center = position + gravity_field.gravity_point_center
		if body.gravity_center == new_center:
			body.set_gravity(position + gravity_field.gravity_point_center, 0, 0)

func void_kill():
	planet_killed.emit(self)
	
