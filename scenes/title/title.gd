extends Node2D

@onready var the_void = $CanvasLayer/Void
@onready var black_screen = $CanvasLayer/BlackScreen
@onready var sfx_slider = $CanvasLayer/SFXSlider
@onready var music_slider = $CanvasLayer/MusicSlider
@onready var main_menu = %MainMenu
@onready var credits_screen = %CreditsScreen
@onready var check_button = $CanvasLayer/MainMenu/CheckButton


func _ready():
	globals.goal_reached = false
	credits_screen.visible = false
	main_menu.visible = true
	black_screen.fade_to_game()
	sfx_slider.value = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("SFX"))
	music_slider.value = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Music"))
	#if not OS.has_feature("web"):
	#	download_button.text = "Save Image"
	if OS.has_feature("android") or OS.has_feature("web_android"):
		globals.mobile_controls = true
		check_button.button_pressed = true


func _on_timer_timeout():
	the_void.burst_particles.position.x = randi_range(-1000, 1000)
	the_void.burst_particles.emitting = true

func _input(event):
	if event.is_action_pressed("Start"):
		_on_start_button_pressed()


func _on_check_button_toggled(toggled_on):
	globals.mobile_controls = toggled_on


func _on_start_button_pressed():
	black_screen.fade_to_black_and_change_to_scene(globals.GAME)


func _on_sfx_slider_value_changed(value):
	if value < -23:
		value = -100
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), value)


func _on_music_slider_value_changed(value):
	if value < -23:
		value = -100
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), value)


func _on_credits_button_pressed():
	main_menu.visible = false
	credits_screen.visible = true


func _on_close_button_pressed():
	main_menu.visible = true
	credits_screen.visible = false
