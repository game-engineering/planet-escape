extends GameCharacter

@export var speed = 50

@onready var animated_sprite_2d = $AnimatedSprite2D
@onready var death_sound = $DeathSound

signal slime_died

var died = false

func _update_control(_delta):
	if died:
		return
	var direction = -1
	animated_sprite_2d.flip_h = direction > 0
	control_velocity = _move_direction(direction) * speed
	animated_sprite_2d.play("walk")
	

func jump_kill():
	if died:
		return
	died = true
	death_sound.play()
	var tween = create_tween()
	tween.tween_property(self, "scale:y", 0, 0.2)
	await death_sound.finished
	slime_died.emit()
	queue_free()



func _on_kill_zone_body_entered(body):
	if not died and body is Player:
		body.enemy_kill()

