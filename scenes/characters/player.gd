class_name Player
extends GameCharacter

signal air_changed(new_value)
signal player_died

@export var speed = 300.0
@export var jump_velocity = 300.0

@onready var animated_sprite_2d = $AnimatedSprite2D
@onready var death_sound = $DeathSound
@onready var jump_sound = $JumpSound

@export var air_consumption : float = 10

var died := false
var joy_direction := 0.0
@export var initial_velocity:=Vector2(0,0)
@export var air : float = 0:
	set(value):
		air = value
		air_changed.emit(value)

func _ready():
	gravity_velocity = initial_velocity

func _update_control(delta):
	if died:
		return

		
	if gravity == 0:
		if air < 0:
			no_air_kill()
		air -= air_consumption * delta
		return
	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		jump_sound.play()
		gravity_velocity += up_direction * jump_velocity
		animated_sprite_2d.play("jump")

	var direction = Input.get_axis("left", "right")
	if joy_direction:
		direction = joy_direction
		
	
		
	if direction:
		animated_sprite_2d.flip_h = direction < 0
		control_velocity = _move_direction(direction) * speed * abs(direction)
		animated_sprite_2d.play("walk")
	else:
		animated_sprite_2d.play("default")
		if control_velocity:
			gravity_velocity += control_velocity / 2
		control_velocity = Vector2(0,0)
			
		


func no_air_kill():
	kill()

func void_kill():
	kill()
	
func enemy_kill():
	kill()

func kill():
	if died:
		return
	death_sound.play()
	died = true
	var tween = create_tween()
	tween.tween_property(self, "scale:y", 0, 1)
	await get_tree().create_timer(2).timeout
	player_died.emit()


func _on_jump_kill_area_body_entered(body):
	if body.has_method("jump_kill"):
		body.jump_kill()
