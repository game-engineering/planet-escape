class_name GameCharacter
extends CharacterBody2D

signal gravity_center_changed(gravity_center: Vector2)

@export var gravity_center = Vector2(0,0)
@export var gravity : float = 0
@export var rotation_speed : float = 0

var planet_position : Vector2
var distance_from_center : float
var circumference_at_height : float

const PI_HALF : float = PI / 2

var gravity_velocity := Vector2(0,0)
var control_velocity := Vector2(0,0)
var rotational_velocity := Vector2(0,0)

func _update_gravity(delta):
	# Add the gravity.
	planet_position = global_position - gravity_center
	#print("PP ", planet_position)
	distance_from_center = planet_position.length()
	#print("DFC ", distance_from_center)
	circumference_at_height = TAU * distance_from_center
	#print("CH ", circumference_at_height)
	up_direction = planet_position.normalized()
	global_rotation = up_direction.angle() + PI_HALF
	#print("Up ", up_direction)
	if not is_on_floor():
		gravity_velocity -= (up_direction * gravity * delta)
		rotational_velocity = 1.32 * _move_direction(1) * (circumference_at_height / 360) * rotation_speed
	else:
		rotational_velocity = Vector2(0,0)
		gravity_velocity = Vector2(0,0)
	#print(rotational_velocity)

func _update_control(_delta):
	pass

func _move_direction(direction: float):
	return up_direction.rotated(PI_HALF * sign(direction))

func _physics_process(delta):
	_update_gravity(delta)
	_update_control(delta)	
	velocity = control_velocity + gravity_velocity + rotational_velocity
	move_and_slide()

func set_gravity(new_center, new_gravity, new_rotation_speed):
	#print("New gravity: ", new_center, " / ", new_gravity)
	gravity_center = new_center
	gravity = new_gravity
	rotation_speed = new_rotation_speed
	if rotational_velocity:
		gravity_velocity += rotational_velocity
	gravity_center_changed.emit(new_center)
