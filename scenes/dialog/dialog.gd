class_name Dialog
extends CanvasLayer

signal finished

@onready var nametag = %Nametag
@onready var sizer : Control = %Sizer
@onready var balloon : PanelContainer = %Balloon
@onready var h_box_container = $MarginContainer/HBoxContainer
@onready var v_box_container = $MarginContainer/HBoxContainer/VBoxContainer
@onready var text_label : Label= %TextLabel
@onready var name_label : Label = %NameLabel

const A = preload("res://voice/a.ogg")
const BA = preload("res://voice/ba.ogg")
const BE = preload("res://voice/be.ogg")
const BI = preload("res://voice/bi.ogg")
const BO = preload("res://voice/bo.ogg")
const BU = preload("res://voice/bu.ogg")
const E = preload("res://voice/e.ogg")
const I = preload("res://voice/i.ogg")
const O = preload("res://voice/o.ogg")
const U = preload("res://voice/u.ogg")

@onready var sounds = $Sounds
@onready var timer = $Timer
@onready var hide_timer = $HideTimer

var audio_bus_name

var nametag_stylebox

@export var hide_time := 1.5:
	set(value):
		hide_time=value
		if hide_timer:
			hide_timer.wait_time = value

@export var pitch : float = 1:
	set(value):
		pitch = value
		if audio_bus_name:
			var bus_index = AudioServer.get_bus_index(audio_bus_name)
			if bus_index != -1 and AudioServer.get_bus_effect_count(bus_index) > 0:
				var pitch_effect : AudioEffectPitchShift = AudioServer.get_bus_effect(bus_index, 0)
				pitch_effect.set("pitch_scale", value)

@export var chars_per_second : int = 40:
	set(value):
		chars_per_second = value
		if timer:
			timer.wait_time = 1.0 / value

@export var nametag_name : String = "Noname":
	set(value):
		nametag_name = value
		if name_label:
			name_label.text = value

@export var nametag_fontcolor : Color = Color(1,1,1):
	set(value):
		nametag_fontcolor = value
		if name_label:
			name_label.add_theme_color_override("font_color", value)

@export var nametag_color : Color :
	set(value):
		nametag_color = value
		if nametag:
			nametag_stylebox.modulate_color = value
			# nametag.has_theme_font_size_override()"theme_override_styles/panel"

@export var horizontal_position : BoxContainer.AlignmentMode = BoxContainer.ALIGNMENT_CENTER:
	set(value):
		horizontal_position = value
		if h_box_container:
			h_box_container.alignment = value
			
@export var vertical_position : BoxContainer.AlignmentMode = BoxContainer.ALIGNMENT_BEGIN:
	set(value):
		vertical_position = value
		if v_box_container:
			v_box_container.alignment = value

func _ready():
	hide_time = hide_time
	nametag_stylebox = nametag.get_theme_stylebox("panel").duplicate()
	nametag.add_theme_stylebox_override("panel", nametag_stylebox)
	nametag_color = nametag_color
	nametag_name = nametag_name
	nametag_fontcolor = nametag_fontcolor
	chars_per_second = chars_per_second
	sizer.custom_minimum_size = balloon.size
	horizontal_position = horizontal_position
	vertical_position = vertical_position
	
	var bus_index = AudioServer.bus_count
	audio_bus_name = str(ResourceUID.create_id())
	AudioServer.add_bus(bus_index)
	AudioServer.set_bus_name(bus_index, audio_bus_name)
	AudioServer.add_bus_effect(bus_index, AudioEffectPitchShift.new(), 0)


	pitch = pitch
	
	for sound in [A, E, I, O, U, BA, BE, BI, BO, BU]:
		var player = AudioStreamPlayer.new()
		player.stream = sound
		player.autoplay = false
		sounds.add_child(player)
		player.bus = audio_bus_name 
		
	timer.timeout.connect(_speak_character)


func speak(text):
	if not hide_timer.is_stopped():
		hide_timer.stop()
	show()
	text_label.text = text
	text_label.visible_characters = 0
	timer.start()

var current_player

func _speak_character():
	if text_label.visible_characters < len(text_label.text):
		text_label.visible_characters += 1
		var character = text_label.text[text_label.visible_characters - 1]
		if current_player:
			current_player.stop()
		current_player = sounds.get_child(randi() % sounds.get_child_count())
		if character != " ":
			current_player.play()
	else:
		timer.stop()
		hide_timer.start()


func _on_hide_timer_timeout():
	finished.emit()
	hide()
