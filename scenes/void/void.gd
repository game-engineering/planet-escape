extends Node2D

@onready var collision_shape_2d = $Area2D/CollisionShape2D
@onready var polygon_2d = $Polygon2D
@onready var gpu_particles_2d = $GPUParticles2D

@export var speed = 10
@export var planet_killing = true
@export var enabled := true
@onready var burst_particles = $BurstParticles
@onready var alert_sound = $AlertSound

func _notification(what):
	if what == NOTIFICATION_PAUSED:
		gpu_particles_2d.interpolate = false
	elif what == NOTIFICATION_UNPAUSED:
		gpu_particles_2d.interpolate = true

func _ready():
	burst_particles.emitting = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if not enabled or get_tree().paused:
		return
	position.y -= speed * delta
	polygon_2d.polygon[2].y += speed * delta
	polygon_2d.polygon[3].y += speed * delta

func create_rectangle(pos: Vector2, size: Vector2):
	var polygon = PackedVector2Array()
	polygon.append(pos)
	polygon.append(pos+Vector2(size.x, 0))
	polygon.append(pos+size)
	polygon.append(pos+Vector2(0, size.y))
	return polygon


func _on_area_2d_body_entered(body):
	if planet_killing and body.has_method("void_kill"):
		alert_sound.play()
		burst_particles.global_position.x = body.position.x
		if "size" in body:
			burst_particles.process_material.emission_box_extents.x = body.size
		else:
			burst_particles.process_material.emission_box_extents.x = 100
		get_tree().create_timer(2).timeout.connect(func(): 
			burst_particles.emitting = true
			alert_sound.stop()
			)
		body.void_kill()
