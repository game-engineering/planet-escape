extends Sprite2D
@onready var dialog: Dialog = $Dialog
@onready var animation_timer = $AnimationTimer

@export var nametag = "???":
	set(value):
		nametag = value
		if dialog:
			dialog.nametag_name = value

@export var message = "I have a message for you."

@export var hide_time := 1.5:
	set(value):
		hide_time = value
		if dialog:
			dialog.hide_time = value

@export var color = Color.LIGHT_BLUE:
	set(value):
		color = value
		if dialog:
			dialog.nametag_color = value
			
func _ready():
	hide_time = hide_time
	nametag = nametag
	color = color
	if hframes > 1 or vframes > 1:
		animation_timer.start()
		animation_timer.timeout.connect(func():
				frame = (frame + 1) % hframes * vframes
				)

func _on_area_2d_body_entered(body):
	if body is Player:
		get_tree().paused = true
		dialog.speak(message)
		await dialog.finished
		get_tree().paused = false
