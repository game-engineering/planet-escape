# Credits

## Sprites
- Alien UFO Pack, Kenney, https://kenney.nl/assets/alien-ufo-pack
- Platformer Pack Redux, Kenney, https://kenney.nl/assets/platformer-pack-redux
- Platformer Art Ext. Enemies, Kenney, https://kenney.nl/assets/platformer-art-extended-enemies 

## Music

- Purple Legacy, CC-BY, Thor Arisland, https://opengameart.org/content/purple-legacy

## SFX
- 8bit-death-whirl, CC0, Fupi, https://opengameart.org/content/8bit-death-whirl
- sfx-jump, CC-BY, Blender Foundation, https://opengameart.org/content/jump-sound-effect-yo-frankie
- Short Wind Sound, CC0, remaxim, https://opengameart.org/content/short-wind-sound
- impsplat, CC0, Independent.nu, https://opengameart.org/content/8-wet-squish-slurp-impacts

## Font
- Belanosima, OFL, The DocRepair Project, Santiago Orozco, https://fonts.google.com/specimen/Belanosima
